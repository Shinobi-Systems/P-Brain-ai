# P-Brain.ai - API-Based Personal Assistant

>This is a modified version of P-Brain.ai for Shinobi CCTV.

>Shinobi Website : http://shinobi.video


>Thanks patrickjquinn for your hard work on P-Brain.ai!

>Original P-Brain.ai Repository : https://github.com/patrickjquinn/P-Brain.ai

# Dependencies

- Shinobi (http://shinobi.video)

# Setup

## Add P-Brain to Shinobi

1. Navigate to your Shinobi folder and clone the modified P-Brain library. Let's assume Shinobi is in the default location.
    ```
    cd /home/Shinobi
    git clone https://gitlab.com/Shinobi-Systems/P-Brain-ai.git libs/customAutoLoad/P-Brain-ai
    ```

2. Navigate to inside the `libs/customAutoLoad/P-Brain-ai` folder and run the following
    ```
    sudo npm install
    ```
    or if it fails to install :
    ```
    npm install --unsafe-perm
    ```

3. Restart Shinobi
    ```
    pm2 restart camera
    ```

## Install - Extra Windows Instructions

Install Python: https://www.python.org/downloads/windows/

Install Node.js v6: https://nodejs.org/en/download/

Install Windows Build Tools: `npm install --global --production windows-build-tools`


# Skills

## Adding Skills

Add a skill by creating a new folder with the name of your new skill and adding an `index.js`.



Add functions for `intent` and `{skill_name}_resp` to that index, the latter contining the logic that will respond to a query. The `{skill_name}_resp` function must have a response type of `String`



In `intent` add `return {keywords:['key 1','key 2'], module:'{skill_name}'}` where `keywords` are the phrases you wish the skill to respond to and `{skill_name}` is the name of your new skill.



Add `module.exports = {intent, get: {skill_name}_resp};` to the end of your `index.js`



Add that new folder to the `skills` directory in the project.



And bang, Brain will automatically import and enable your new skill!

### API

For more detail on adding skills see the Wiki page [Adding Skills](https://github.com/patrickjquinn/P-Brain.ai/wiki/Adding-Skills).

# Clients

### Web Client
`http://localhost:4567/api/ask?q={query}`
`http://localhost:4567/`
`http://localhost:4567/settings.html`
`http://localhost:4567/users.html`
