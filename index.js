global.pBrain = {}
const co = require('co')
const express = require('express')
const wrap = require('co-express')
const pBrainApp = express()
const http = require('http').Server(pBrainApp)
const pBrainSocketIo = require('socket.io')(http, {'pingInterval': 2000, 'pingTimeout': 7000})
const compression = require('compression')
const fs = require('fs')
const ip = require('ip')
const liblibDir = __dirname + '/'
const search = require(liblibDir + 'api/core-ask.js')
const skills = require(liblibDir + 'skills/skills.js')
const settingsApi = require(liblibDir + 'api/settings.js')
const usersApi = require(liblibDir + 'api/users.js')
const cookieParser = require('cookie-parser')
global.pBrain.auth = require(liblibDir + 'authentication')
const Database = require(liblibDir + 'db')
var proxy = require('express-http-proxy')
module.exports = function(s,config,lang,app,io){
    pBrainApp.use(compression({
        threshold: 0,
        level: 9,
        memLevel: 9
    }))

    pBrainApp.use((req, res, next) => {
        req.connection.setNoDelay(true)
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'X-Requested-With')
        next()
    })

    pBrainApp.use(cookieParser())

    pBrainApp.get('/api/status', wrap(function * (req, res) {
        res.json({status:200,msg:'OK'})
    }))
    pBrainApp.use('/', [global.pBrain.auth.filter(true), express.static(liblibDir + 'src')])
    pBrainApp.use('/api/settings', [global.pBrain.auth.filter(false), settingsApi])
    pBrainApp.use('/api/users', [global.pBrain.auth.filter(false), usersApi])
    pBrainApp.get('/api/user', global.pBrain.auth.filter(false), wrap(function * (req, res) {
        res.json(req.user)
    }))
    pBrainApp.get('/api/token', global.pBrain.auth.filter(false), wrap(function * (req, res) {
        res.json(req.token)
    }))

    pBrainApp.get('/api/status', global.pBrain.auth.filter(false), wrap(function * (req, res) {
        res.json({status:200,msg:'OK'})
    }))

    // TODO parse services in query
    pBrainApp.get('/api/ask', global.pBrain.auth.filter(false), wrap(function * (req, res) {
        const input = req.query.q.toLowerCase()

        try {
            const result = yield search.query(input, req.user, req.token)
            res.json(result)
        } catch (e) {
            console.log(e)
            res.json({msg: {text: 'Sorry, I didn\'t understand ' + input}, type: 'error'})
        }
    }))

    pBrainApp.get('/api/login', global.pBrain.auth.login)
    pBrainApp.get('/api/logout/:user?', [global.pBrain.auth.filter(false), global.pBrain.auth.logout])
    pBrainApp.get('/api/tokens/:user?', [global.pBrain.auth.filter(false), global.pBrain.auth.viewTokens])
    pBrainApp.get('/api/validate', [global.pBrain.auth.filter(false), global.pBrain.auth.validate])
    pBrainSocketIo.use(global.pBrain.auth.verifyIO)

    pBrainSocketIo.on('connect', socket => {
        co(function *() {
            socket.on('ask', co.wrap(function *(msg) {
                try {
                    const result = yield search.query(msg, socket.user, socket.token)
                    socket.emit('response', result)
                } catch (e) {
                    console.log(e)
                    socket.emit('response', {msg: {text: 'Sorry, I didn\'t understand ' + msg.text.toLowerCase()}, type: 'error'})
                }
            }))
            yield skills.registerClient(socket, socket.user)
        }).catch(err => {
            console.log(err)
        })
    })

    function * initialSetup() {
        if ((yield global.pBrain.db.getGlobalValue('port')) == null) {
            console.log('Setting default global values in database')
            yield global.pBrain.db.setGlobalValue('port', 4567)
            yield global.pBrain.db.setGlobalValue('promiscuous_mode', true)
            yield global.pBrain.db.setGlobalValue('promiscuous_admins', true)
        }
    }

    co(function * () {
        console.log('Setting up database.')
        global.pBrain.db = yield Database.setup()
        yield initialSetup()

        global.pBrain.sendToUser = function (user, type, message) {
            const sockets = global.pBrain.auth.getSocketsByUser(user)
            sockets.map(socket => {
                socket.emit(type, message)
            })
        }
        global.pBrain.sendToDevice = function(token, type, message) {
            const socket = global.pBrain.auth.getSocketByToken(token)
            if (socket) {
                socket.emit(type, message)
            } else {
                console.log("Failed to send to device: " + JSON.stringify(token))
            }
        }

        console.log('Loading skills.')
        yield skills.loadSkills()
        console.log('Training recognizer.')
        yield search.train_recognizer(skills.getSkills())
        console.log('Starting server.')
        const port = yield global.pBrain.db.getGlobalValue('port')
        http.listen(port, () => {
            console.log(`Server started on http://localhost:${port}`)
        })
        app.use(config.webPaths.apiPrefix+':auth/aiSearch/:ke/', function(req,res,next){
            if(s.auth(req.params,function(user){
                proxy(`http://127.0.0.1:${port}/api`)(req,res)
            },res,req) === false){
                next(req,res)
            }
        })

        const promiscuous = yield global.pBrain.db.getGlobalValue('promiscuous_mode')
        const promiscuous_admins = yield global.pBrain.db.getGlobalValue('promiscuous_admins')
        if (promiscuous) {
            console.log('Warning! Promiscuous mode is enabled all logins will succeed.')
            if (promiscuous_admins) {
                console.log('Possibly deadly warning! Promiscuous admins is enabled.' +
                    ' All new users will be admins and can view each others data.')
            }
            console.log(`Settings can be changed at http://localhost:${port}/settings.html`)
        }
    }).catch(err => {
        console.log(err)
        throw err
    })
}
